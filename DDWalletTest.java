public class DDWalletTest {
    private DDWallet walletAccount;

    @Before
    public void setUp() {
        // Create a DDWallet object with initial values for testing
        walletAccount = new DDWallet("987654321", "Alice", 30000000.0); // Initial balance: Rp30,000,000
    }

    // TODO 3: Buat method test saat kondisi balance cukup untuk melakukan pay
    @Test
    public void testPayWithSufficientBalance() {

    }

    // TODO 4: Buat method test saat kondisi balance tidak cukup untuk melakukan pay
    @Test
    public void testPayWithInsufficientBalance() {

    }

    // TODO 5: Buat method test untuk mengecek balance setelah 3 bulan
    @Test
    public void testAddInterestWithBalanceAfterThreeMonths() {

    }
}
