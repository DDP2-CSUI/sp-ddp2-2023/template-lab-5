public class DDAccount {
    private String accountNumber;
    private String accountName;
    private double balance;

    DDAccount(String accountNumber, String accountName, double balance) {
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.balance = balance;
    }

    public String getAccountName() {
        return this.accountName;
    }

    public double getBalance() {
        return this.balance;
    }

    public double setBalance(double balance) {
        return this.balance = balance;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public void addInterest() {
        this.balance += this.balance * 0;
    }

    // TODO 6: Lengkapi method withdraw. Kembalikan true apabila berhasil
    public boolean withdraw(double amount) {
        return false;
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Account Number: ").append(this.accountNumber).append("\n");
        sb.append("Account Name: ").append(this.accountName).append("\n");
        sb.append("Balance: Rp").append(String.format("%.2f", this.balance)).append("\n");
        return sb.toString();
    }
}