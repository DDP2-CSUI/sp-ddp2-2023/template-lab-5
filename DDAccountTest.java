public class DDAccountTest {
    DDAccount account;

    @Before
    public void setUp() {
        // Create new DDAccount
        account = new DDAccount("123456789", "John Doe", 1_000_000.0);
    }

    // TODO 1: Buat method test saat kondisi balance cukup untuk melakukan withdraw
    @Test
    public void testWithdrawSufficientBalance() {

    }

    // TODO 2: Buat method test saat kondisi balance tidak cukup untuk melakukan withdraw
    @Test
    public void testWithdrawInsufficientBalance() {
        
    }
}
